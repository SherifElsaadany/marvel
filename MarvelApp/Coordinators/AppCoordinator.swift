//
//  AppCoordinator.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import UIKit

class AppCoordinator: Coordinator {
    
    //MARK:- properties
    var navigationController: UINavigationController
    
    //MARK:- methods
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        showCharactersListScene()
    }
    
    func showCharactersListScene() {
        navigationController.setViewControllers([CharactersListConfigurator().configureViewController(delegate: self)], animated: true)
    }
    
    func showDetailsScene(character: ResultDetails) {
        let vc = DetailsVC()
        vc.character = character
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showSearchScene(characters: Characters) {
        navigationController.pushViewController(SearchConfigurator().configureViewController(characters: characters, delegate: self) , animated: false)
    }
}

//MARK:- CharactersListSceneDelegate
extension AppCoordinator: CharactersListSceneDelegate {
    func onDidSelect(character: ResultDetails) {
        showDetailsScene(character: character)
    }
    
    func didTapSearch(characters: Characters) {
        showSearchScene(characters: characters)
    }
}

//MARK:- SearchSceneDelegate
extension AppCoordinator: SearchSceneDelegate {}
