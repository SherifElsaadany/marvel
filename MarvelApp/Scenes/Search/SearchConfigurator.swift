//
//  SearchConfigurator.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

struct SearchConfigurator {
    func configureViewController(characters: Characters, delegate: SearchSceneDelegate) -> SearchVC {
        let vc = SearchVC()
        let presenter = SearchPresenter(characters: characters)
        presenter.delegate = delegate
        vc.presenter = presenter
        return vc
    }
}
