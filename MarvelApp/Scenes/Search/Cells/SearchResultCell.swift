//
//  SearchResultCell.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import UIKit

class SearchResultCell: UITableViewCell, ReusableView {
    
    //MARK:- outlets
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    //MARK:- life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- methods
    func configure(with character: CharactersViewData?) {
        nameLabel.text = character?.name ?? ""
        if let url = URL(string: character?.imageData ?? "") {
            characterImage.kf.setImage(with: url)
        }
    }
}
