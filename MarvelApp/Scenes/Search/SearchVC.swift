//
//  SearchVC.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import UIKit

class SearchVC: UIViewController {
    
    //MARK:- properties
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    var presenter: SearchPresentation?
    private var characters: [CharactersViewData]?
    
    //MARK:- outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.onViewDidLoad(view: self)
        configureSearchBar()
        setTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSearchNavBar(with: searchBar)
        setCancelBtn()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.onViewDidDisappear()
    }
    
    //MARK:- methods
    func configureSearchBar() {
        searchBar.delegate = self
        searchBar.searchTextField.becomeFirstResponder()
    }
    
    func setCancelBtn() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))
        self.navigationItem.rightBarButtonItem?.tintColor = .red
    }
    
    @objc func cancelTapped() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func setTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        registerCell()
    }
    
    func registerCell() {
        tableView.register(UINib(nibName: SearchResultCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: SearchResultCell.reuseIdentifier)
    }

}

//MARK:- UISearchBarDelegate
extension SearchVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.queryDidChange(to: searchText)
    }
}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        characters?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.reuseIdentifier) as! SearchResultCell
        let character = characters?[indexPath.row]
        cell.configure(with: character)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.onDidSelect(index: indexPath.row)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
}

//MARK:- SearchScene
extension SearchVC: SearchScene {
    func setRows(with characters: [CharactersViewData]) {
        self.characters = characters
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
