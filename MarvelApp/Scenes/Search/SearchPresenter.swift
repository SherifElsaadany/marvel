//
//  SearchPresenter.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

class SearchPresenter {
    //MARK:- properties
    private var characters: Characters
    private var filteredCharacters: [ResultDetails]?
    private weak var view: SearchScene?
    weak var delegate: SearchSceneDelegate?
    
    //MARK:- init
    init(characters: Characters) {
        self.characters = characters
    }
    
    //MARK:- methods
    private func createViewData(of characters: [ResultDetails]) {
        var mappedCharacters = [CharactersViewData]()
        for character in characters {
            let character = CharactersViewData(name: character.name ?? "", imageData: "\(character.thumbnail?.path ?? "")/standard_medium.\(character.thumbnail?.thumbnailExtension ?? "")")
            mappedCharacters.append(character)
        }
        setViewRows(with: mappedCharacters)
    }
    
    private func setViewRows(with characters: [CharactersViewData]) {
        view?.setRows(with: characters)
    }
}

extension SearchPresenter: SearchPresentation {
    func onViewDidLoad(view: SearchScene) {
        self.view = view
    }
    
    func onViewDidDisappear() {
        self.view = nil
    }
    
    func onDidSelect(index: Int) {
        guard let character = filteredCharacters?[index] else { return }
        delegate?.onDidSelect(character: character)
    }
    
    func queryDidChange(to query: String) {
        let filteredCharacters = characters.data?.results?.filter({
            $0.name?.contains(query) ?? false
        })
        self.filteredCharacters = filteredCharacters
        createViewData(of: filteredCharacters ?? [ResultDetails]())
    }
}
