//
//  SearchProtocols.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

protocol SearchScene: class {
    func setRows(with characters: [CharactersViewData])
}

protocol SearchPresentation {
    func onViewDidLoad(view: SearchScene)
    func onViewDidDisappear()
    func onDidSelect(index: Int)
    func queryDidChange(to query: String)
}

protocol SearchSceneDelegate: class {
    func onDidSelect(character: ResultDetails)
}
