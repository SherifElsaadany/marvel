//
//  CharacterCell.swift
//  MarvelApp
//
//  Created by Elsaadany on 02/04/2021.
//

import UIKit
import Kingfisher

class CharacterCell: UITableViewCell, ReusableView {

    //MARK:- outlets
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK:- life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK:- methods
    func configure(with character: CharactersViewData?) {
        nameLabel.text = character?.name ?? ""
        if let url = URL(string: character?.imageData ?? "") {
            characterImage.kf.setImage(with: url)
        }
    }
}
