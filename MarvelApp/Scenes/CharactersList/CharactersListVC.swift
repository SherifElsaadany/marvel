//
//  CharactersListVC.swift
//  MarvelApp
//
//  Created by Elsaadany on 02/04/2021.
//

import UIKit
import Moya

class CharactersListVC: UIViewController {
    
    //MARK:- properties
    private let provider = MoyaProvider<MarvelService>()
    var presenter: CharactersListPresentation?
    private var characters: [CharactersViewData]?
        
    //MARK:- outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        presenter?.onViewDidLoad(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setMarvelNavBar()
        setSearchBtn()
        presenter?.onViewWillAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setSearchBtn() {
        var image = UIImage(named: "icn-nav-search")
        image = image?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(searchTapped))
    }
    
    @objc func searchTapped() {
        presenter?.didTapSearch()
    }
    
    func setTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        registerCell()
    }
    
    func registerCell() {
        tableView.register(UINib(nibName: CharacterCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: CharacterCell.reuseIdentifier)
    }
    
}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension CharactersListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        characters?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CharacterCell.reuseIdentifier) as! CharacterCell
        let character = characters?[indexPath.row]
        cell.configure(with: character)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (tableView.bounds.width * 140) / 190
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.onDidSelect(index: indexPath.row)
    }
}

//MARK:- CharacterListScene
extension CharactersListVC: CharacterListScene {
    func setRows(with characters: [CharactersViewData]) {
        self.characters = characters
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
