//
//  CharactersListPresenter.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

class CharactersListPresenter {
    
    //MARK:- properties
    private var service: CharactersServiceProtocol
    private weak var view: CharacterListScene?
    weak var delegate: CharactersListSceneDelegate?
    private var fetchedCharacters: Characters?
    
    //MARK:- init
    init(service: CharactersServiceProtocol) {
        self.service = service
    }
    
    //MARK:- methods
    private func getCharacters() {
        service.getCharacters { (result) in
            switch result {
            case .success(let characters):
                self.fetchedCharacters = characters
                self.createViewData(of: characters)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func createViewData(of characters: Characters) {
        var mappedCharacters = [CharactersViewData]()
        guard let results = characters.data?.results else { return }
        for character in results {
            let character = CharactersViewData(name: character.name ?? "", imageData: "\(character.thumbnail?.path ?? "")/landscape_large.\(character.thumbnail?.thumbnailExtension ?? "")")
            mappedCharacters.append(character)
        }
        setViewRows(with: mappedCharacters)
    }
    
    private func setViewRows(with characters: [CharactersViewData]) {
        view?.setRows(with: characters)
    }
}

//MARK:- CharactersListPresentation
extension CharactersListPresenter: CharactersListPresentation {
    
    func onViewDidLoad(view: CharacterListScene) {
        self.view = view
        getCharacters()
    }
    
    func onViewWillAppear() {
        getCharacters()
    }
    
    func onViewDidDisappear() {
        self.view = nil
    }
    
    func onDidSelect(index: Int) {
        guard let character = fetchedCharacters?.data?.results?[index] else { return }
        delegate?.onDidSelect(character: character)
    }
    
    func didTapSearch() {
        guard let characters = fetchedCharacters else { return }        
        delegate?.didTapSearch(characters: characters)
    }
}
