//
//  CharactersListConfigurator.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

struct CharactersListConfigurator {
    func configureViewController(delegate: CharactersListSceneDelegate) -> CharactersListVC {
        let vc = CharactersListVC()
        let presenter = CharactersListPresenter(service: CharactersService())
        presenter.delegate = delegate
        vc.presenter = presenter
        return vc
    }
}
