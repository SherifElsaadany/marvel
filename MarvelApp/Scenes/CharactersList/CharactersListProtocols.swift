//
//  CharactersListProtocols.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

protocol CharacterListScene: class {
    func setRows(with characters: [CharactersViewData])
}

protocol CharactersListPresentation {
    func onViewDidLoad(view: CharacterListScene)
    func onViewWillAppear()
    func onViewDidDisappear()
    func onDidSelect(index: Int)
    func didTapSearch()
}

protocol CharactersListSceneDelegate: class {
    func didTapSearch(characters: Characters)
    func onDidSelect(character: ResultDetails)
}
