//
//  DetailsVC.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import UIKit
import Kingfisher

class DetailsVC: UIViewController {
    
    //MARK:- properties
    var character: ResultDetails?
    
    let headerImage = UIImageView()
    
    //MARK:- outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
    }
    
    //MARK:- methods
    func setTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        registerCell()
        updateHeaderImage()
    }
    
    func registerCell() {
        tableView.register(UINib(nibName: imageCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: imageCell.reuseIdentifier)
    }
    
    func updateHeaderImage() {
        let urlStr = "\(character?.thumbnail?.path ?? "")/standard_large.\(character?.thumbnail?.thumbnailExtension ?? "")"
        guard let url = URL(string: urlStr) else { return }
        headerImage.kf.setImage(with: url)
    }
    
    func createHeaderLabel(with text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textColor = .red
        label.font = label.font.withSize(13)
        return label
    }

}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension DetailsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: imageCell.reuseIdentifier) as! imageCell
        cell.configure(with: character?.comics)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return headerImage
        default:
            return createHeaderLabel(with: "Comics")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return tableView.bounds.width
        default:
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        300
    }
}
