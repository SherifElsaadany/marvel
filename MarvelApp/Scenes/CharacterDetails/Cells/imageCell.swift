//
//  imageCell.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import UIKit

class imageCell: UITableViewCell, ReusableView {
    
    //MARK:- properties
    private var comics: Comics?

    //MARK:- outlets
    @IBOutlet weak var collectinView: UICollectionView!
    
    //MARK:- life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        collectinView.delegate = self
        collectinView.dataSource = self
        registerCells()
    }
    
    func registerCells() {
        collectinView.register(UINib(nibName: DetailsCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: DetailsCell.reuseIdentifier)
    }
    
    func configure(with comics: Comics?) {
        self.comics = comics
    }
}

extension imageCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        comics?.items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailsCell.reuseIdentifier, for: indexPath) as! DetailsCell
        cell.configure(with: comics?.items?[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.bounds.height
        
        return CGSize(width: height * 0.5, height: height)
    }
}
