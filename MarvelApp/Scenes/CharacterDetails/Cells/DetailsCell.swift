//
//  DetailsCell.swift
//  MarvelApp
//
//  Created by Elsaadany on 04/04/2021.
//

import UIKit
import Kingfisher

class DetailsCell: UICollectionViewCell, ReusableView {

    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(with comic: ComicsItem?) {
        let urlStr = "\(comic?.resourceURI ?? "")/standard_medium"
        
        if let url = URL(string: urlStr) {
            self.characterImage.kf.setImage(with: url)
        }
        nameLabel.text = comic?.name ?? ""
    }
}
