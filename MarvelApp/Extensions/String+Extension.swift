//
//  String+Extension.swift
//  MarvelApp
//
//  Created by Elsaadany on 02/04/2021.
//

import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

extension String {
    var md5: String {
        let data = self.data(using: .utf8)
        var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))

        _ = data!.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) in
            return CC_MD5(bytes.baseAddress, CC_LONG(data!.count), &digest)
        }

        return digest.reduce(into: "") { $0 += String(format: "%02x", $1) }
    }
}

