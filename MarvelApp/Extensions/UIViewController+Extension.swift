//
//  UIViewController+Extension.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import UIKit

extension UIViewController {
    func setMarvelNavBar() {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .black
        
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        
        let logo = UIImage(named: "icn-nav-marvel")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    func setSearchNavBar(with searchBar: UISearchBar) {
        self.navigationItem.setHidesBackButton(true, animated: false)
        searchBar.placeholder = "Search"
        searchBar.tintColor = .red
        searchBar.searchTextField.textColor = .white
        navigationItem.titleView = searchBar
    }
}
