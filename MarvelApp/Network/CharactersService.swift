//
//  CharactersService.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation
import Moya

class CharactersService: CharactersServiceProtocol {
    
    var provider = MoyaProvider<MarvelService>()
    
    func getCharacters(completion: @escaping ((Result<Characters, Error>) -> Void)) {
        provider.request(.characters) { (result) in
            switch result {
            case .success(let response):
                
                do {
                    let characters = try JSONDecoder().decode(Characters.self, from: response.data)
                    completion(.success(characters))
                } catch {
                    print("paring error \(error)")
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
