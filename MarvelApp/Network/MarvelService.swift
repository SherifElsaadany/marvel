//
//  MarvelService.swift
//  MarvelApp
//
//  Created by Elsaadany on 02/04/2021.
//

import Foundation
import Kingfisher
import Moya

enum MarvelService {
    
    //auth keys
    static private let publicKey = "bb261ecda31f68a691564301ecada942"
    static private let privateKey = "c6649f41055c53bacd48372c19b4286a283e7e74"
    
    //service cases
    case characters
}

//MARK:- TargetType
extension MarvelService: TargetType {
    var baseURL: URL {
        return URL(string: "https://gateway.marvel.com")!
    }
    
    var path: String {
        switch self {
        case .characters:
            return "/v1/public/characters"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        
        let ts = "\(Date().timeIntervalSince1970)"
        let hash = (ts + MarvelService.privateKey + MarvelService.publicKey).md5
        let authParams: [String : Any] = ["apikey": MarvelService.publicKey, "ts": ts, "hash": hash]
        
        switch self {
        case .characters:
            return .requestParameters(parameters: authParams, encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
}
