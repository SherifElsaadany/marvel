//
//  CharactersServiceProtocol.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

protocol CharactersServiceProtocol {
    func getCharacters(completion: @escaping ((Result<Characters, Error>) -> Void))
}
