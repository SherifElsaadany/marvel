//
//  ReusableView.swift
//  MarvelApp
//
//  Created by Elsaadany on 02/04/2021.
//

import Foundation

// Object, that adopts this protocol, will use identifier that matches name of its class.
protocol ReusableView: class {}

extension ReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
