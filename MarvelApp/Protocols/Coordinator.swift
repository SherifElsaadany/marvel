//
//  Coordinator.swift
//  MarvelApp
//
//  Created by Elsaadany on 02/04/2021.
//

import UIKit

protocol Coordinator: class {
    var navigationController: UINavigationController { get }
    
    func start()
    func popViewController(animated: Bool)
    func dismissViewController(animated: Bool, completion: (() -> Void)?)
}

extension Coordinator {
    
    func popViewController(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    
    func dismissViewController(animated: Bool, completion: (() -> Void)?) {
        navigationController.dismiss(animated: animated, completion: completion)
    }
}

