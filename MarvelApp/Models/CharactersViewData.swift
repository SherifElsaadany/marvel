//
//  CharactersViewData.swift
//  MarvelApp
//
//  Created by Elsaadany on 03/04/2021.
//

import Foundation

struct CharactersViewData {
    var name: String
    var imageData: String?
}
